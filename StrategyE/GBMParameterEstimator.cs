﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyE
{
    class GBMParameterEstimator
    {
        private Queue<double> logReturns;
        private double previousPrice;
        private double volatility;

        public GBMParameterEstimator()
        {
            logReturns = new Queue<double>();
            previousPrice = 0.0d;
        }

        public double addPrice(double price)
        {
            if (logReturns.Count > 30)
            {
                logReturns.Dequeue();
            }

            if (previousPrice != 0.0d)
            {
                logReturns.Enqueue(Math.Log(price) - Math.Log(previousPrice));
            }

            previousPrice = price;

            if (logReturns.Count > 30)
            {
                //return estimateVolatility();
                volatility = nonCenteredVolatility();
                return volatility;
            }
            else
            {
                return 0.0d;
            }
        }

        public double estimateVolatility()
        {
            double mean = 0.0;
            double sum = 0.0;
            double stdDev = 0.0;
            int n = 0;
            foreach (double val in logReturns)
            {
                n++;
                double delta = val - mean;
                mean += delta / n;
                sum += delta * (val - mean);
            }
            if (1 < n)
                stdDev = Math.Sqrt(sum / (n - 1));

            return stdDev;
        }

        public double nonCenteredVolatility()
        {
            double sum = 0.0;
            double stdDev = 0.0;
            int n = 0;
            foreach (double val in logReturns)
            {
                n++;
                sum += val * val;
            }
            if (1 < n)
                stdDev = Math.Sqrt(sum / n);

            return stdDev;
        }

        public double getVolatility()
        {
            return volatility;
        }
    }
}
