﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyE
{
    class Bar
    {
        private List<double> ticks;
        public double open, high, low, close;

        public Bar()
        {
            open = double.NaN;
            high = double.NegativeInfinity;
            low = double.PositiveInfinity;
            close = double.NaN;
            ticks = new List<double>();
        }

        public void addTick(double tick)
        {
            if (double.IsNaN(open))
                open = tick;
            if (tick > high)
                high = tick;
            if (tick < low)
                low = tick;
            close = tick;
        }
    }
}
