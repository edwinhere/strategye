﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using RGiesecke.DllExport;
using System;

namespace StrategyE
{
    public class Main
    {
        private static List<PointFigure> charts;
        private static List<GBMParameterEstimator> estimators;
        private static List<Bar> bars;

        [DllExport("createTool", CallingConvention = CallingConvention.StdCall)]
        public static int createTool(double reversal)
        {
            var pf = new PointFigure(reversal);
            var e = new GBMParameterEstimator();
            var b = new Bar();

            if (charts == null)
            {
                charts = new List<PointFigure>();
            }

            if (estimators == null)
            {
                estimators = new List<GBMParameterEstimator>();
            }

            if (bars == null)
            {
                bars = new List<Bar>();
            }

            charts.Add(pf);
            estimators.Add(e);
            bars.Add(b);
            return charts.Count - 1;
        }

        [DllExport("newTick", CallingConvention = CallingConvention.StdCall)]
        public static int newTick(int handle, double bid, double offer)
        {
            double tick = (bid + offer) / 2.0;
            bars[handle].addTick(tick);
            double volatility = charts[handle].getBoxSize();
            if (volatility > 0.0d)
            {
                charts[handle].update(Math.Log(tick));
                int action = charts[handle].getAction();
                return action;
            }
            else
            {
                return 0;
            }
        }

        [DllExport("newBar", CallingConvention = CallingConvention.StdCall)]
        public static double newBar(int handle)
        {
            var volatility = estimators[handle].addPrice(bars[handle].close);
            bars[handle] = new Bar();
            if (volatility > 0.0)
            {
                charts[handle].setBoxSize(volatility);
            }

            return Math.Exp(volatility);
        }

        [DllExport("getOutlook", CallingConvention = CallingConvention.StdCall)]
        public static int getOutlook(int handle)
        {
            return charts[handle].getOutlook();
        }

        [DllExport("cleanEverything", CallingConvention = CallingConvention.StdCall)]
        public static void cleanEverything()
        {
            charts.Clear();
            estimators.Clear();
            bars.Clear();
            System.GC.Collect();
        }

        [DllExport("cleanTool", CallingConvention = CallingConvention.StdCall)]
        public static void cleanTool(int handle)
        {
            charts[handle].blankOut();
            charts[handle] = null;
            estimators[handle] = null;
            bars[handle] = null;
            System.GC.Collect();
        }
    }
}