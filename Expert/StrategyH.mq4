//+------------------------------------------------------------------+
//|                                                    StrategyG.mq4 |
//|                           Copyright 2015, Edwin Jose Palathinkal |
//|                                     https://github.com/edwinhere |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Edwin Jose Palathinkal"
#property link      "https://github.com/edwinhere"
#property version   "1.00"
#property strict

#import "StrategyE.dll"
int createTool(double reversal);
int newTick(int handle, double bid, double offer);
double newBar(int handle);
int getOutlook(int handle);
void cleanEverything();
void cleanTool(int handle);
#import

#define MAX 9

int dllHandle, outlookHandle;
int curIndex;
datetime times[MAX];
double box;
bool active = false;

int utils_periodToPeriodIndex(int period) {
  switch(period) {
    case PERIOD_M1  : return(0); break;
    case PERIOD_M5  : return(1); break;
    case PERIOD_M15 : return(2); break;
    case PERIOD_M30 : return(3); break;
    case PERIOD_H1  : return(4); break;
    case PERIOD_H4  : return(5); break;
    case PERIOD_D1  : return(6); break;
    case 20         : return(7); break;
    case 55         : return(8); break;
  }
  
  return -1;
}

int utils_periodIndexToPeriod(int index) {
  switch(index) {
    case 0: return(PERIOD_M1); break;
    case 1: return(PERIOD_M5); break;
    case 2: return(PERIOD_M15); break;
    case 3: return(PERIOD_M30); break;
    case 4: return(PERIOD_H1); break;
    case 5: return(PERIOD_H4); break;
    case 6: return(PERIOD_D1); break;
    case 7: return(20); break;
    case 8: return(55); break;
  }
  
  return -1;
}

void OnInit() {
  active = false;
  dllHandle = createTool(1.0);
  outlookHandle = createTool(1.0);
  curIndex = utils_periodToPeriodIndex(Period());
  times[curIndex] = Time[0];
  for(int i=curIndex+1; i<MAX; i++) {
    times[i] = times[curIndex]- MathMod(times[curIndex],utils_periodIndexToPeriod(i)*60);
  }
}

void OnDeinit(const int reason) {
  active = true;
  cleanTool(dllHandle);
  cleanTool(outlookHandle);
}

void OnTick() {
  int action = newTick(dllHandle, Bid, Ask);
  newTick(outlookHandle, Bid, Ask);
  int outlook = getOutlook(outlookHandle);
  if (times[curIndex] != Time[0]) {
    times[curIndex] = Time[0];
    onBar(Period());
    for(int i = curIndex+1; i<MAX; i++) {
      int period = utils_periodIndexToPeriod(i),
      seconds = period*60,
      time0 = times[curIndex] - MathMod(times[curIndex],seconds);
      if (times[i] != time0) {
        times[i] = time0;
        onBar(period);
      }
    }
  }
  
  double spread = Ask - Bid;
  //active = OrdersOpen() > 0;
  if(box > 0.0 && action != 0 && outlook != 0 && spread < 10 * Point/* && !active*/) {
    if(outlook == 1 && action == 1) {
      OrderSend(
         Symbol(),
         OP_BUYSTOP,
         Lot(),
         NormalizeDouble(Ask, 5),
         0,
         NormalizeDouble(Ask / MathPow(box, 2.0), 5),
         NormalizeDouble(Bid * MathPow(box, 3.0), 5));
    } else if(outlook == -1 && action == -1) {
      OrderSend(
         Symbol(),
         OP_SELLSTOP,
         Lot(),
         NormalizeDouble(Bid, 5),
         0,
         NormalizeDouble(Bid * MathPow(box, 2.0), 5),
         NormalizeDouble(Ask / MathPow(box, 3.0), 5));
    }
  }
}

void onBar(int period) {
  if(period == PERIOD_H1) {
    double volatility = newBar(dllHandle);
    if(volatility > 0.0) {
      box = volatility;
    }
    return;
  } else if(period == PERIOD_H4) {
    newBar(outlookHandle);
    return;
  }
}

int OrdersOpen(int iExpertID=-1) {
  int iTotal=OrdersTotal(), iOrders, i;
  
  for(i=0; i < iTotal; i++) {                                       // For all orders of the terminal
    if(!OrderSelect(i, SELECT_BY_POS)) return(-1);
    if(iExpertID<0 || OrderMagicNumber()==iExpertID) {
      if(OrderCloseTime()==0) iOrders++;
    }
  }
  return(iOrders);
}

double Lot() {
  double OneLotMargin = MarketInfo(Symbol(),MODE_MARGINREQUIRED);
  double FreeMargin = AccountFreeMargin();
  double lotMM = FreeMargin/OneLotMargin*5/100;
  double LotStep = MarketInfo(Symbol(),MODE_LOTSTEP);
  lotMM = NormalizeDouble(lotMM/LotStep,0)*LotStep;
  return lotMM;
}